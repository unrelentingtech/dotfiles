let-env PWD_STACK = []
let-env PWD_POPPING = false

let-env config = {
  history: { file_format: sqlite }
  table: { mode: light }
  completions: {
    # algorithm: fuzzy
  }
  cd: { abbreviations: true }
  ls: {
    use_ls_colors: true
    clickable_links: true
  }
  shell_integration: true
  show_banner: false
  hooks: {
    env_change: {
      PWD: { |before, after|
        let-env PWD_STACK = if $before != $nothing and $env.PWD_POPPING == false { ($env.PWD_STACK | append $before) } else { $env.PWD_STACK }
        let-env PWD_POPPING = false # must be here because of when the hook actually runs
      }
    }
  }
  keybindings: [
    {
       name: trigger-completion-menu
       modifier: none
       keycode: tab
       mode: emacs
       event: { 
         until: [
           { send: menu name: completion_menu }
           { send: menunext }
           { edit: complete }
         ]
       }
    }
    {
      name: fuzzy_history
      modifier: CONTROL
      keycode: Char_r
      mode: emacs
      event: {
        send: executehostcommand
        cmd: "commandline (history | each { |it| $it.command } | uniq | reverse | str collect (char nl) | pick -q (commandline) | decode utf-8 | str trim)"
      }
    }
    {
      name: proj_jump
      modifier: CONTROL
      keycode: Char_j
      mode: emacs
      event: {
        send: executehostcommand
        cmd: "cd (rhq list | pick -q (commandline) | decode utf-8 | str trim); commandline ''"
      }
    }
    {
      name: vsplit
      modifier: CONTROL
      keycode: Char_v
      mode: emacs
      event: {
        send: executehostcommand
        cmd: "wezterm cli split-pane --right -- nu -c (commandline); commandline ''"
      }
    }
    {
      name: hsplit
      modifier: CONTROL
      keycode: Char_s
      mode: emacs
      event: {
        send: executehostcommand
        cmd: "wezterm cli split-pane --bottom -- nu -c (commandline); commandline ''"
      }
    }
  ]
}

alias p = (
  let-env PWD_POPPING = true;
  cd ($env.PWD_STACK | last);
  let-env PWD_STACK = ($env.PWD_STACK | drop);
)

alias l   = exa -FbTlL1 --icons --no-permissions --no-user --no-time
alias l2  = exa -FbTlL2 --icons --no-permissions --no-user --no-time
alias l3  = exa -FbTlL3 --icons --no-permissions --no-user --no-time
alias l4  = exa -FbTlL4 --icons --no-permissions --no-user --no-time
alias l5  = exa -FbTlL5 --icons --no-permissions --no-user --no-time
alias la  = exa -FbTlL1 --icons --no-permissions --no-user --no-time -a
alias la2 = exa -FbTlL2 --icons --no-permissions --no-user --no-time -a
alias la3 = exa -FbTlL3 --icons --no-permissions --no-user --no-time -a
alias la4 = exa -FbTlL4 --icons --no-permissions --no-user --no-time -a
alias la5 = exa -FbTlL5 --icons --no-permissions --no-user --no-time -a
alias ll  = exa -FbTlL1 --icons --octal-permissions --git --links -a
alias ll2 = exa -FbTlL2 --icons --octal-permissions --git --links -a
alias ll3 = exa -FbTlL3 --icons --octal-permissions --git --links -a
alias ll4 = exa -FbTlL4 --icons --octal-permissions --git --links -a
alias ll5 = exa -FbTlL5 --icons --octal-permissions --git --links -a

alias g = git
alias pick = sk --layout=reverse --height=40%
alias my = cd ~/src/codeberg.org/valpackett/

# Make a directory and cd into it
def-env mcd [name: path] { mkdir $name; cd $name }

# Generate a Low-Quality Image Preview data URL
def-env clqip [name: path] {
  "data:image/webp;base64," + (cwebp -q 5 -m 6 -resize 12 10 -quiet $name -o /dev/stdout | encode base64)
}
