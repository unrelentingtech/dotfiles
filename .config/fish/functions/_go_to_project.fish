function _go_to_project --description 'Use broot to navigate to a project directory'
    set -l cmd_file (mktemp)
    if broot --outcmd $cmd_file --only-folders --conf ~/.config/broot/cd_picker.hjson ~/src
        read --local --null cmd <$cmd_file
        rm -f $cmd_file
        eval $cmd
        commandline --function repaint
    else
        set -l code $status
        rm -f $cmd_file
        commandline --function repaint
        return $code
    end
end
