function fish_prompt
    set_color grey
    printf '%s' (prompt_pwd (string replace /usr$HOME $HOME $PWD))
    set_color normal
    printf '❯ '
end
