function withgmake
    mkdir -p $XDG_RUNTIME_DIR/withgmake
    ln -sf (which gmake) $XDG_RUNTIME_DIR/withgmake/make
    set -lx PATH $XDG_RUNTIME_DIR/withgmake:$PATH
    $argv
end
