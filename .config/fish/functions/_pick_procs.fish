# Mostly https://github.com/PatrickF1/fzf.fish/blob/main/functions/_fzf_search_processes.fish
# Only guaranteed to work with FUZZY=sk (https://github.com/lotabout/skim)

function _pick_procs --description "Search all running processes. Replace the current token with the pid of the selected process."
    set -f ps_cmd (command -v ps || echo "ps")
    set -f ps_preview_fmt (string join ',' 'pid' 'ppid=PARENT' 'user' '%cpu' 'rss=RSS_IN_KB' 'start=START_TIME' 'command')
    set -f processes_selected (
        $ps_cmd -A -opid,command | \
        $FUZZY --ansi --multi --header-lines=1 --height=85% --reverse \
            --query (commandline --current-token) \
            --prompt="pid> " --preview="$ps_cmd -ww -o '$ps_preview_fmt' -p (string match -r '\d+' \"{}\") || echo 'Cannot preview:' (string match -r '\d+' \"{}\")" \
            --preview-window="down:6:wrap"
    )

    if test $status -eq 0
        for process in $processes_selected
            set -f --append pids_selected (string split --no-empty --field=1 -- " " $process)
        end
        commandline --current-token --replace -- (string join ' ' $pids_selected)
    end

    commandline --function repaint
end
