local wezterm = require 'wezterm'

-- local theme = 'Synthwave (Gogh)'
local theme = 'Tokyo Night Moon'

local spawn_tab_next = function(window, pane)
	-- https://github.com/wez/wezterm/discussions/2691
	local mux_window = window:mux_window()
	local tabs = mux_window:tabs_with_info()
	local current_index = 0
	for _, tab_info in ipairs(tabs) do
		if tab_info.is_active then
			current_index = tab_info.index
			break
		end
	end
	mux_window:spawn_tab{}
	window:perform_action(wezterm.action.MoveTab(current_index+1), pane)
end

return {
	color_scheme = theme,
	font = wezterm.font("0xProto"),
	font_rules = {
		{ intensity = 'Half', italic = true, font = wezterm.font("Monaspace Radon Light") },
		{ intensity = 'Half', font = wezterm.font("Monaspace Krypton ExtraLight") },
		{ italic = true, font = wezterm.font("Monaspace Radon") },
	},
	font_size = 15,
	line_height = 1.3,
	underline_position = '200%',
	freetype_load_target = 'Light',
	freetype_load_flags = 'NO_HINTING',
	integrated_title_button_style = "Gnome",
	integrated_title_buttons = { "Close" },
	xcursor_theme = "Adwaita",
	xcursor_size = 24,
	window_decorations = "INTEGRATED_BUTTONS | RESIZE",
	window_padding = { top = 0, right = 0, bottom = 0, left = 0 },
	window_frame = {
		font = wezterm.font("Cantarell"),
		font_size = 14,
		active_titlebar_bg = "#303030",
		inactive_titlebar_bg = "#242424",
	},
	colors = {
		tab_bar = {
			active_tab = {
				bg_color = wezterm.color.get_builtin_schemes()[theme].background,
				fg_color = wezterm.color.get_builtin_schemes()[theme].foreground,
			}
		},
		visual_bell = wezterm.color.parse(wezterm.color.get_builtin_schemes()[theme].background):lighten(0.2):saturate(0.1),
	},
	visual_bell = {
		fade_in_function = 'EaseIn',
		fade_in_duration_ms = 150,
		fade_out_function = 'EaseOut',
		fade_out_duration_ms = 150,
	},
	scrollback_lines = 65536,
	exit_behavior = "Close",
	-- front_end = "WebGpu",
	enable_wayland = true,
	enable_kitty_graphics = true,
	use_resize_increments = true,
	unicode_version = 14,
	warn_about_missing_glyphs = false,
	quick_select_alphabet = "arstqwfpzxcvneioluymdhgjbk",
	mouse_bindings = {
		{ event = {Up  ={streak=1, button="Left"}}, mods = "NONE", action = wezterm.action{CompleteSelection="PrimarySelection"} },
		{ event = {Up  ={streak=1, button="Left"}}, mods = "CTRL", action = "OpenLinkAtMouseCursor" },
		{ event = {Down={streak=1, button="Left"}}, mods = "CTRL", action = "Nop" },
		{ event = {Down={streak=4, button="Left"}}, mods = "NONE", action = {SelectTextAtMouseCursor="SemanticZone"} },
	},
	key_map_preference = "Physical",
	leader = { key = "q", mods = "CTRL", timeout_milliseconds = 800 },
	keys = {
		{ key = ".", mods = "CTRL", action = wezterm.action{CharSelect={copy_on_select=false}} },
		{ key = "e", mods = "CTRL|SHIFT", action = wezterm.action{Search={CaseSensitiveString=""}} }, -- colemak:f
		{ key = "f", mods = "CTRL|SHIFT", action = wezterm.action_callback(spawn_tab_next) }, -- colemak:t
		{ key = "c", mods = "LEADER", action = wezterm.action{SpawnTab="CurrentPaneDomain"} },
		{ key = "x", mods = "LEADER", action = wezterm.action{CloseCurrentPane={confirm=true}} },
		{ key = "v", mods = "LEADER", action = wezterm.action{SplitHorizontal={domain="CurrentPaneDomain"}} },
		{ key = "d", mods = "LEADER", action = wezterm.action{SplitVertical={domain="CurrentPaneDomain"}} }, -- colemak:s
		{ key = "r", mods = "LEADER", action = wezterm.action{ActivateTabRelative=-1} }, -- colemak:p
		{ key = "j", mods = "LEADER", action = wezterm.action{ActivateTabRelative=1} }, -- colemak:n
		{ key = "h", mods = "LEADER", action = wezterm.action{ActivatePaneDirection="Left"} },
		{ key = "n", mods = "LEADER", action = wezterm.action{ActivatePaneDirection="Down"} }, -- colemak:k
		{ key = "y", mods = "LEADER", action = wezterm.action{ActivatePaneDirection="Up"} }, -- colemak:j
		{ key = "u", mods = "LEADER", action = wezterm.action{ActivatePaneDirection="Right"} },  --colemak:l
		{ key = "<", mods = "LEADER", action = wezterm.action{AdjustPaneSize={"Left", 2}} },
		{ key = "-", mods = "LEADER", action = wezterm.action{AdjustPaneSize={"Down", 2}} },
		{ key = "=", mods = "LEADER", action = wezterm.action{AdjustPaneSize={"Up", 2}} },
		{ key = "+", mods = "LEADER", action = wezterm.action{AdjustPaneSize={"Up", 2}} },
		{ key = ">", mods = "LEADER", action = wezterm.action{AdjustPaneSize={"Right", 2}} },
		{ key = "/", mods = "LEADER", action = wezterm.action{Search={CaseInSensitiveString=""}} },
		{ key = "?", mods = "LEADER", action = wezterm.action{Search={CaseInSensitiveString=""}} },
		{ key = "UpArrow",   mods = "SHIFT", action = wezterm.action{ScrollToPrompt=-1} },
		{ key = "DownArrow", mods = "SHIFT", action = wezterm.action{ScrollToPrompt=1} },
		{ key = "Return", mods = "ALT", action = wezterm.action.DisableDefaultAssignment },
	},
	-- mux_env_remove = {},
	-- unix_domains = {
	-- 	{ name = "unix", connect_automatically = true },
	-- },
}
