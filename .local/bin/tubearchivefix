#!/usr/bin/env ruby
# tubearchivefix by Val Packett (available under the Unlicense)
#
# This script carefully finds and fixes issues with a youtube-dl/yt-dlp YouTube archive directory:
# - if there are files with multiple names for the same ID, the older names are deleted,
#   unless the newest name only has video files that have incorrect duration
# - if there are multiple video formats for the same ID, one is kept, preferring ones with correct duration,
#   then preferring higher resolutions and better containers (webm/opus >* mkv > mp4)
#   * why is webm better if it's internally the same as mkv? accepts free formats only + playable in browsers
# - partial downloads, annotations xml, description are removed
# - files are renamed to the new yt-dlp default template (ID in square brackets)
#
# The archive MUST contain .info.json files (--write-info-json)!
#
# My download flags:
# -P temp:/tmp --write-info-json --add-metadata --write-thumbnail --write-subs --merge-output-format webm -S vcodec:av01,acodec:opus

require 'date'
require 'json'

PATH = ARGV[0]
DRY_RUN = ENV['DRY_RUN'] == '1'
DEL_SINGLE_BAD_DUR = ENV['DEL_SINGLE_BAD_DUR'] == '1'

def files_at(path)
  Dir.entries(path)
    .select { |f| File.file? File.join(path, f) }
end

def colorize(val, color)
  case color
    when :red then "\e[31m" + val.to_s + "\e[0m"
    when :green then "\e[32m" + val.to_s + "\e[0m"
    when :yellow then "\e[33m" + val.to_s + "\e[0m"
    when :blue then "\e[34m" + val.to_s + "\e[0m"
    when :magenta then "\e[35m" + val.to_s + "\e[0m"
    when :cyan then "\e[36m" + val.to_s + "\e[0m"
    when :bright_red then "\e[1m\e[31m" + val.to_s + "\e[0m"
    when :bright_green then "\e[1m\e[32m" + val.to_s + "\e[0m"
    when :bright_yellow then "\e[1m\e[33m" + val.to_s + "\e[0m"
    when :bright_blue then "\e[1m\e[34m" + val.to_s + "\e[0m"
    when :bright_magenta then "\e[1m\e[35m" + val.to_s + "\e[0m"
    when :bright_cyan then "\e[1m\e[36m" + val.to_s + "\e[0m"
    else val.to_s
  end
end

def is_vid?(ext)
  ext == '.webm' || ext == '.mkv' || ext == '.mp4' || ext == '.opus'
end

def basename(name)
  # Unfortunately titles sometimes may contain dots.
  # I don't like hardcoding a list of things that are extensions so this regex technically
  # may catch normal parts of the title. But in practice they seem rare enough.
  #
  # example: this will turn even something like: (very contrived test case)
  # 'Somehow I Have To Make This Engine Look New.. _ Ford 4.0 OHV V6 [CvRgX2X_w_8].f123.part.annotations.xml.en-CA.mp4.webm'
  # into: 'Somehow I Have To Make This Engine Look New.. _ Ford 4.0 OHV V6 [CvRgX2X_w_8]'
  name.split('.').reverse.drop_while { |p| p.match? /^([a-zA-Z0-9]{1,4}|[a-zA-Z]{2}-[a-zA-Z]{2}|annotations|description|live_chat)$/ }.reverse.join('.')
end

def vid_width(path)
  ((JSON.parse(IO.popen(['ffprobe', '-v', 'quiet', path, '-print_format', 'json', '-select_streams', 'v:0', '-show_entries', 'stream=width']).read)['streams'] || []).first || {})['width']
end

def vid_duration(path)
  (JSON.parse(IO.popen(['ffprobe', '-v', 'quiet', path, '-print_format', 'json', '-show_entries', 'format=duration']).read)['format'] || {})['duration'].to_f
end

def meta_duration(path)
  meta = JSON.parse(File.read(path))
  return meta['duration'] if meta['duration'].is_a? Numeric
  unless meta['duration_string'].is_a? String
    puts colorize("  no duration in '#{path}'", :red)
    return -1
  end
  delems = meta['duration_string'].split(':').reverse
  seconds = delems.shift.to_i
  seconds += delems.shift.to_i * 60 unless delems.empty?
  seconds += delems.shift.to_i * 60 * 60 unless delems.empty?
  seconds
end

def find_meta_duration(files)
  info = files.find { |f| f.end_with? '.info.json' }
  return -1 if info.nil?
  meta_duration(File.join(PATH, info))
end

def any_correct_length_vid?(files)
  mdur = find_meta_duration files
  if mdur < 0
    puts "  no info among #{files}"
    return false
  end
  files.any? { |f|
    next false unless is_vid?(File.extname(f).downcase)
    vdur = vid_duration(File.join(PATH, f))
    next true if (vdur - mdur).abs < 3
    puts "  bad duration on '#{f}': expected ~#{mdur} seconds, got #{vdur}"
    false
  }
end

def meta_viewcount(path)
  JSON.parse(File.read(path))['view_count']
end

def delete_old_names(vid, vfiles)
  # assuming case-insensitive FS like exfat, so downcase
  basenames = vfiles.map { |f| basename f }.uniq { |f| f.downcase }
  if basenames.length > 1
    puts "#{colorize vid, :cyan}: #{colorize 'multiple video names found!', :red}"
    cbasenames = basenames.map { |bn|
      infopath = File.join(PATH, bn + '.info.json')
      unless File.file? infopath
        puts colorize("  '#{infopath}' not found! bailing on old name deletion", :bright_red)
        return vfiles
      end
      [bn, meta_viewcount(infopath)]
    }.sort_by { |cb| -cb.last }
    unless any_correct_length_vid?(vfiles.select { |f| f.start_with? cbasenames.first.first })
      puts colorize("  no length-correct video for newest name '#{cbasenames.first.first}'! bailing on old name deletion", :bright_red)
      return vfiles
    end
    cbasenames.each do |cb|
      bn = cb.first
      is_first = bn == cbasenames.first.first
      puts "  - '#{bn}': #{colorize cb.last, (is_first ? :green : :red)} views"
      next if is_first
      vfiles.select! do |f|
        if f.start_with? bn
          puts "    #{colorize 'deleting', :bright_magenta} '#{f}'"
          File.delete File.join(PATH, f) unless DRY_RUN
          false
        else
          true
        end
      end
    end
  end
  vfiles
end

def delete_unnecessary(vid, vfiles)
  vfiles.select do |f|
    if f.end_with?('.part') || f.end_with?('.ytdl') || f.end_with?('.tmp') || f.end_with?('.annotations.xml') || f.end_with?('.description')
      puts "#{colorize vid, :cyan}: #{colorize 'deleting', :bright_magenta} '#{f}'"
      File.delete File.join(PATH, f) unless DRY_RUN
      false
    else
      true
    end
  end
end

def rename_to_dlp_style(vid, vfiles)
  vfiles.map do |f|
    nf = f.gsub /-([0-9a-zA-Z\-_]{11})\./, ' [\1].'
    if f != nf
      puts "#{colorize vid, :cyan}: #{colorize 'renaming', :bright_yellow} '#{f}' #{colorize 'to', :bright_yellow} '#{nf}'"
      if File.file? File.join(PATH, nf)
        # really should be handled by delete_old_names but check just in case
        puts colorize("  file exists! bailing on dlp style renaming and everything", :bright_red)
        return [] # and do no further processing in this case
      end
      File.rename File.join(PATH, f), File.join(PATH, nf) unless DRY_RUN
    end
    DRY_RUN ? f : nf
  end.uniq
end

def delete_duplicate_formats(vid, vfiles)
  vidfiles = vfiles.select { |f| is_vid?(File.extname(f).downcase) }
  if vidfiles.empty?
    puts "#{colorize vid, :cyan}: #{colorize 'no video files found!', :red} only #{vfiles.map { |f| File.extname f }.join ', '}"
    return vfiles
  end
  mdur = find_meta_duration vfiles
  if mdur < 0
    puts "#{colorize vid, :cyan}: #{colorize 'no duration found in metadata! bailing on deleting duplicate formats / duration checking', :bright_red}"
    return vfiles
  end
  if vidfiles.length == 1
    vdur = vid_duration File.join(PATH, vidfiles.first)
    unless (vdur - mdur).abs < 3
      puts "#{colorize vid, :cyan}: #{colorize 'bad duration on', :red} '#{vidfiles.first}': expected ~#{mdur} sec, found #{vdur} sec"
      if DEL_SINGLE_BAD_DUR
        puts "  #{colorize 'deleting', :bright_magenta} '#{vidfiles.first}'"
        File.delete File.join(PATH, vidfiles.first) unless DRY_RUN
      end
    end
    return vfiles
  end
  puts "#{colorize vid, :cyan}: #{colorize 'multiple video formats found!', :red} expected duration ~#{mdur} sec"
  vidfiles.map do |f|
    vdur = vid_duration File.join(PATH, f)
    valid_dur = (vdur - mdur).abs < 3
    score = valid_dur ? 1000000 : 0
    score += vid_width(File.join(PATH, f)) || 0
    score += 2 if File.extname(f) == '.webm' || File.extname(f) == '.opus'
    score += 1 if File.extname(f) == '.mkv'
    puts "  - '#{f}' (#{colorize "#{vdur} sec", (valid_dur ? :green : :red)}) - score #{score}"
    [f, score]
  end.sort_by { |vf| -vf.last }.drop(1).each do |vf|
    puts "  #{colorize 'deleting', :bright_magenta} '#{vf.first}'"
    File.delete File.join(PATH, vf.first) unless DRY_RUN
  end
end

if PATH.nil?
  puts "provide a directory path"
  exit 1
end

vids = files_at(PATH).each_with_object({}) { |f, h|
  m = f.match(/\s\[([0-9a-zA-Z\-_]{11})\]\./) || f.match(/-([0-9a-zA-Z\-_]{11})\./)
  next if m.nil?
  h[m[1]] ||= []
  h[m[1]].push f
}

vids.each do |vid, vfiles|
  vfiles = delete_unnecessary vid, vfiles
  vfiles = delete_old_names vid, vfiles
  vfiles = rename_to_dlp_style vid, vfiles
  delete_duplicate_formats vid, vfiles
end
